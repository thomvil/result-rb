# frozen_string_literal: true

# Result can hold a success value in an `Ok` or and error in an `Err`
class Result
    attr_reader :ok, :err_description, :err_value

    def initialize(ok_val, err_description, err_value, type)
        @ok, @err_description, @err_value, @type = ok_val, err_description, err_value, type
    end

    def self.ok(value)
        new(value, nil, nil, :ok)
    end

    def self.err(description:, value:)
        raise ArgumentError, 'The error description should be a symbol' unless description.is_a? Symbol

        new(nil, description, value, :err)
    end

    def ok?
        @type == :ok
    end

    def err?
        @type == :err
    end

    def contains?(val)
        @ok == val
    end

    def contains_err?(description, value)
        @err_description == description && @err_value == value
    end

    def map
        return self if err?

        Result.ok(yield @ok)
    end

    def map_or(val)
        err? ? val : yield(@ok)
    end

    def map_or_else(map:, or_else:)
        err? ? or_else.call(@err_description, @err_value) : map.call(@ok)
    end

    def map_err
        return self if ok?

        descr, val = yield @err_description, @err_value
        Result.err(description: descr, value: val)
    end

    def and(other_res)
        err? ? self : other_res
    end

    def and_then
        return self if err?

        new_res = yield @ok
        raise "The provided block should return a Result, not a #{new_res.class}" unless new_res.is_a? Result

        self.and new_res
    end

    def or(other_res)
        ok? ? self : other_res
    end

    def or_else
        return self if ok?

        new_res = yield @err_description, @err_value
        raise "The provided block should return a Result, not a #{new_res.class}" unless new_res.is_a? Result

        self.or new_res
    end

    def unwrap_or(ok_value)
        err? ? ok_value : @ok
    end

    def unwrap_or_else
        err? ? yield(@err_description, @err_value) : @ok
    end

    def unwrap
        raise unwrap_runtime_error_msg if err?

        @ok
    end

    def unwrap_err
        raise unwrap_err_runtime_error_msg if ok?

        [@err_description, @err_value]
    end

    def ==(other)
        @ok == other.ok && @err_description == other.err_description && @err_value == err_value
    end

    def ===(other)
        self == other
    end

    def inspect
        ok? ? "Result::Ok(#{@ok.inspect})" : "Result::Err(#{@err_description.inspect}, #{@err_value.inspect})"
    end

    private

    def unwrap_runtime_error_msg
        "\nCan't unwrap a Result::Err\n    #{@err_description}\n    #{@err_value}"
    end

    def unwrap_err_runtime_error_msg
        "\nCan't unwrap_err a Result::Ok\n    #{@ok}"
    end

    private_class_method :new
    alias is_ok ok?
    alias is_err err?
    alias contains contains?
    alias contains_err contains_err?
end

require_relative './result/core_ext/object'
