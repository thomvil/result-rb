# frozen_string_literal: true

# Convenience-constructors for `Result`
class Object
    # rubocop:disable Naming/MethodName
    def Ok(value)
        Result.ok(value)
    end

    def Err(description, value)
        Result.err(description: description, value: value)
    end
    # rubocop:enable Naming/MethodName
end
