# frozen_string_literal: true

require './lib/rresult'
require './lib/result/core_ext/object'

describe Result do
    describe '::new' do
        it 'fails' do
            expect { Result.new(nil, nil, nil, nil) }.to raise_error(NoMethodError)
        end
    end

    describe '#type' do
        it 'fails' do
            expect { Result.ok(nil).type }.to raise_error(NoMethodError)
        end
    end

    describe '::ok' do
        it 'returns a Result::Ok' do
            expect(Result.ok('foobar').ok?).to be_truthy
            expect(Result.ok('foobar').is_ok).to be_truthy
            expect(Result.ok('foobar').err?).to be_falsey
            expect(Result.ok('foobar').is_err).to be_falsey
            expect(Result.ok('foobar').contains('foobar')).to be_truthy
            expect(Result.ok('foobar').contains?('foobar')).to be_truthy
        end
    end

    describe '::Err' do
        it 'returns a Result::Err' do
            expect(Err(:missing_key, :user).err?).to be_truthy
            expect(Err(:missing_key, :user).is_err).to be_truthy
            expect(Err(:missing_key, :user).ok?).to be_falsey
            expect(Err(:missing_key, :user).is_ok).to be_falsey
            expect(Err(:missing_key, :user).contains_err(:missing_key, :user)).to be_truthy
            expect(Err(:missing_key, :user).contains_err?(:missing_key, :user)).to be_truthy
        end

        context 'given anything other than a symbol as description' do
            it 'raises an ArgumentError' do
                expect { Err('missing_key', :user) }.to raise_error(ArgumentError)
            end
        end
    end

    describe 'Object' do
        context 'convenience-constructors' do
            it 'works as more verbose constructors' do
                expect(Ok(nil).ok?).to be_truthy
                expect(Ok('foobar').ok?).to be_truthy
                expect(Ok('foobar').unwrap).to be == 'foobar'
                expect(Err(:missing_key, :user).contains_err?(:missing_key, :user)).to be_truthy
            end
        end
    end

    describe '#map' do
        context 'for a Result::Ok' do
            it 'applies the block to wrapped value' do
                expect(Ok('foobar').map(&:upcase).unwrap).to be == 'FOOBAR'
            end
        end

        context 'for a Result::Err' do
            it 'returns itself' do
                res = Err(:missing_key, :user)
                expect(res.map.map(&:upcase)).to be == res
            end
        end
    end

    describe '#map_or' do
        context 'for a Result::Ok' do
            it 'maps the wrapped value' do
                expect(Ok('foobar').map_or(42, &:length)).to be == 6
            end
        end

        context 'for a Result::Err' do
            it 'returns the provided alternative value' do
                res = Err(:missing_key, :user)
                expect(res.map_or(42, &:length)).to be == 42
            end
        end
    end

    describe '#map_or_else' do
        context 'for a Result::Ok' do
            it 'maps the wrapped value' do
                map = ->(ok) { ok.length }
                or_else = ->(_desc, _val) { 42 }
                expect(Ok('foobar').map_or_else(map: map, or_else: or_else)).to be == 6
            end
        end

        context 'for a Result::Err' do
            it 'maps the error to a value' do
                map = ->(ok) { ok.length }
                or_else = ->(_desc, _val) { 42 }
                res = Err(:missing_key, :user)
                expect(res.map_or_else(map: map, or_else: or_else)).to be == 42
            end
        end
    end

    describe '#map_err' do
        context 'for a Result::Ok' do
            it 'returns itself' do
                res = Ok('foobar')
                expect(res.map_err { |_descr, _val| [:new_label, 'baz'] }).to be == res
            end
        end

        context 'for a Result::Err' do
            it 'maps the error' do
                res = Err(:missing_key, :user)
                new_res = Err(:new_label, :USER)
                expect(res.map_err { |_descr, val| [:new_label, val.upcase] }).to be == new_res
            end
        end
    end

    describe '#and' do
        context 'for a Result::Ok' do
            it 'applies logical AND with other Result' do
                res1 = Ok('foobar')
                res2 = Ok('foobaz')
                err = Err(:missing_key, :user)
                expect(res1.and(res2)).to be == res2
                expect(res1.and(err)).to be == err
            end
        end

        context 'for a Result::Err' do
            it 'returns itself' do
                res = Ok('foobar')
                err1 = Err(:missing_key, :user)
                err2 = Err(:missing_key, :password)
                expect(err1.and(err2)).to be == err1
                expect(err1.and(res)).to be == err1
            end
        end
    end

    describe '#and_then' do
        context 'for a Result::Ok' do
            it 'applies logical AND with a generated Result' do
                res = Ok('foobar')
                expect(res.and_then { |val| Ok(val.length) }).to be == Ok(6)
                expect(res.and_then { |val| Ok(val.length) }.contains?(6)).to be_truthy
                expect(res.and_then { |val| Err(:missing_key, val) }.contains_err(:missing_key, 'foobar')).to be_truthy
            end
        end

        context 'for a Result::Err' do
            it 'returns itself' do
                res = Err(:missing_key, :user)
                expect(res.and_then { |val| Ok(val.length) }).to be == res
                expect(res.and_then { |val| Err(:missing_key, val) }).to be == res
            end
        end

        context 'given a block that does not return a Result' do
            it 'raises a RuntimeError' do
                expect { Ok('foobar').and_then { |_val| 42 } }.to raise_error(RuntimeError)
            end
        end
    end

    describe '#or' do
        context 'for a Result::Ok' do
            it 'returns itself' do
                res1 = Ok('foobar')
                res2 = Ok('foobaz')
                err = Err(:missing_key, :user)
                expect(res1.or(res2)).to be == res1
                expect(res1.or(err)).to be == res1
            end
        end

        context 'for a Result::Err' do
            it 'applies logical OR with other Result' do
                res = Ok('foobar')
                err1 = Err(:missing_key, :user)
                err2 = Err(:missing_key, :password)
                expect(err1.or(err2)).to be == err2
                expect(err1.or(res)).to be == res
            end
        end
    end

    describe '#or_else' do
        context 'for a Result::Ok' do
            it 'returns itself' do
                res = Ok('foobar')
                expect(res.or_else { |_descr, val| Ok(val.length) }).to be == res
                expect(res.or_else { |_descr, val| Err(:missing_key, val) }.contains?('foobar')).to be_truthy
            end
        end

        context 'for a Result::Err' do
            it 'rapplies logical OR with a generated Result' do
                res = Err(:missing_key, :user)
                expect(res.or_else { |_descr, val| Ok(val.length) }).to be == Ok(4)
                expect(res.or_else { |_descr, val| Err(:missing_key, val.upcase) }).to be == Err(:missing_key, :USER)
            end
        end

        context 'given a block that does not return a Result' do
            it 'raises a RuntimeError' do
                res = Err(:missing_key, :user)
                expect { res.or_else { |_descr, _val| 42 } }.to raise_error(RuntimeError)
            end
        end
    end

    describe '#unwrap_or' do
        context 'for a Result::Ok' do
            it 'returns the wrapped value' do
                expect(Ok('foobar').unwrap_or(42)).to be == 'foobar'
            end
        end

        context 'for a Result::Err' do
            it 'returns the provided default value' do
                expect(Err(:missing_key, :user).unwrap_or(42)).to be == 42
            end
        end
    end

    describe '#unwrap_or_else' do
        context 'for a Result::Ok' do
            it 'returns the wrapped value' do
                expect(Ok('foobar').unwrap).to be == 'foobar'
            end
        end

        context 'for a Result::Err' do
            it 'returns the calculated value' do
                expect(Err(:missing_key, :user).unwrap_or_else { |_descr, val| val.length }).to be == 4
            end
        end
    end

    describe '#unwrap' do
        context 'for a Result::Ok' do
            it 'returns the wrapped value' do
                expect(Ok('foobar').unwrap).to be == 'foobar'
                expect(Ok(nil).unwrap).to be_nil
            end
        end

        context 'for a Result::Err' do
            it 'raises an RuntimeError' do
                expect { Err(:missing_key, :user).unwrap }.to raise_error(RuntimeError)
            end
        end
    end

    describe '#unwrap_err' do
        context 'for a Result::Ok' do
            it 'raises an RuntimeError' do
                expect { Ok('foobar').unwrap_err }.to raise_error(RuntimeError)
            end
        end

        context 'for a Result::Err' do
            it 'returns the wrapped error' do
                expect(Err(:missing_key, :user).unwrap_err).to be == %i[missing_key user]
            end
        end
    end
end
