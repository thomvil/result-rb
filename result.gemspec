# frozen_string_literal: true

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'result/version'

Gem::Specification.new do |s|
    s.name        = 'rresult'
    s.version     = Result::VERSION
    s.authors     = ['Thomas Villa']
    s.email       = ['thomvil87@gmail.com']

    s.summary     = 'A ruby port of the Rust Result enum.'
    s.description = "A ruby port of the Rust Result enum. 🦀\n Documentation will be added later. For now visit https://gitlab.com/thomvil/result-rb for usage"
    s.homepage    = 'http://gitlab.com/thomvil/result-rb'
    s.license     = 'MIT'

    s.files = `git ls-files -z`.split("\x0")
                               .reject { |f| f.match?(%r{^(test|spec|features)/}) || f.match?(/\.code-workspace$/) }

    s.require_paths         = ['lib']
    s.required_ruby_version = '>= 3.0'

    s.add_development_dependency 'rspec', '~> 3.10'
end
